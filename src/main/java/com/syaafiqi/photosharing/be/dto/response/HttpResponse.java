package com.syaafiqi.photosharing.be.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.tomcat.util.http.parser.HttpParser;

@Data
@AllArgsConstructor
public class HttpResponse<T> {
    private boolean status = true;
    private String message = null;
    private T data = null;

    public HttpResponse(final T data) {
        this.data = data;
    }

    public HttpResponse(final boolean status, final String message) {
        this.status = status;
        this.message = message;
    }
}

