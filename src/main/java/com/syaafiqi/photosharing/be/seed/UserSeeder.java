package com.syaafiqi.photosharing.be.seed;

import com.syaafiqi.photosharing.be.dto.pojo.UserDto;
import com.syaafiqi.photosharing.be.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserSeeder implements CommandLineRunner {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    public UserSeeder(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        // Check if the admin user exists
        if (userService.getUserByUsername("admin") == null) {
            // Create admin user
            UserDto adminUser = new UserDto();
            adminUser.setUsername("admin");
            adminUser.setEmail("admin@mail.com");
            adminUser.setPassword("admin");
            userService.createUser(adminUser);
            System.out.println("Default admin user created.");
        } else {
            System.out.println("Admin user already exists.");
        }
    }
}
